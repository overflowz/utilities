import type { IResult } from './interface';

export const tryCatch = <T extends (...args: any[]) => any, E extends Error = Error>(fn: T): IResult<ReturnType<T>, E> => {
  try {
    return { success: true, value: fn() };
  } catch (error) {
    return { success: false, error };
  }
};

export const to = <T, E extends Error = Error>(promise: Promise<T>): Promise<IResult<T, E>> => {
  return promise.then(
    value => ({ success: true, value }),
    error => ({ success: false, error }),
  );
};
